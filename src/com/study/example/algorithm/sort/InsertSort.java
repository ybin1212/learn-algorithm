package com.study.example.algorithm.sort;

import java.util.Arrays;

/**
 * 插入排序.
 *
 * @author yangbin
 **/
public class InsertSort {

    /**
     * 1. 对于给定的一组记录, 初始时分设第一个记录自成一个有序序列, 其余记录为无序序列.
     * 2. 接着从第二个记录开始, 按照记录的大小依次将当前处理的记录插入到其之前的有序序列中, 直至最后一个记录插入到有序序列中为止.
     **/
    public static void insertSort(int[] arr) {

        if (arr != null && arr.length > 0) {

            for (int i = 1, length = arr.length; i < length; i++) {

                int temp = arr[i], j = i;
                if (arr[j - 1] > temp) {

                    while (j >= 1 && arr[j - 1] > temp) {

                        arr[j] = arr[--j];
                    }
                }

                arr[j] = temp;
                System.out.println(toString(arr, i));
            }
        }
    }

    public static String toString(int[] a, int index) {
        if (a == null) {
            return "null";
        }
        int iMax = a.length - 1;
        if (iMax == -1) {
            return "[]";
        }

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(a[i]);
            if (i == index) {
                b.append(']');
            }
            if (i == iMax) {
                return b.toString();
            }
            b.append(", ");
        }
    }

    public static void main(String[] args) {

        int[] arr = {7, 3, 19, 40, 4, 7, 1};

        insertSort(arr);

        System.out.println(Arrays.toString(arr));
    }
}
